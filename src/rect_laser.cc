#include "ros/ros.h"
#include "ros/package.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/LaserScan.h"
#include <utility>
#include <cmath>
#include <sstream>
#include <string>
#include <fstream>

ros::Subscriber sub;
ros::Publisher pub;
std::vector<float> load_ranges; // ranges loaded from a file

void load_data()
{
  // open file
  std::string path = ros::package::getPath("unior_scanning")+"/config/upd_ranges.txt";
  std::ifstream in(path, std::ifstream::in);

  std::string str;
  // Read the next line from File untill it reaches the end.
  while (std::getline(in, str))
  {
    // ROS_INFO("I'm in loop");
    // Line contains string of length > 0 then save it in vector
    if (str.size() > 0)
    {
      load_ranges.push_back(std::stof(str)); // convert to float before push_back
    }
  }

}

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
  sensor_msgs::LaserScan msg;
  msg.header.stamp = scan->header.stamp;
  msg.header.frame_id = "base_scan";      // frame of default scanner
  msg.angle_min = scan->angle_min;
  msg.angle_max = scan->angle_max;
  msg.angle_increment = scan->angle_increment;
  msg.time_increment = scan->time_increment;
  msg.range_min = scan->range_min;
  msg.range_max = scan->range_max;
  msg.ranges.resize(scan->ranges.size());
  msg.intensities.resize(scan->intensities.size());

  for (int i=0; i<scan->ranges.size() ; i++)
  {

    if ((21 <= i) && (i <= 51))  // range of intersection with plane
    {
      if (scan->ranges[i] < (load_ranges[i] - 0.2)) // -0.2 to remove noise from range values
        msg.ranges[i] = scan->ranges[i];  // remove intersection area
      else
        msg.ranges[i] = std::numeric_limits<float>::infinity();
    } 
    else 
    {
      msg.ranges[i] = scan->ranges[i];           // do not change the rest of rays
    }
  }
  
  pub.publish(msg);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rectificated_laser");
  ros::NodeHandle n;
  // Topic initialization
  std::string scan_topic, remap_topic;
  remap_topic = "/rectificated_scan";
  scan_topic = "/scan";
  // Load scan data
  load_data();
  // Publisher/Subscriber initialization
  pub = n.advertise<sensor_msgs::LaserScan>(remap_topic, 50);
  sub = n.subscribe(scan_topic, 1, scanCallback);
  
  ros::spin();
  
  return 0;
}
