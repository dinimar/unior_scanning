#include "scan_control.h"

ScanControl::ScanControl(ros::NodeHandle &n,
                         std::string controlPubTopic,
                         std::string scanSubTopic,
                         uint32_t queueSize)
{
  m_controlPub = n.advertise<ackermann_msgs::AckermannDriveStamped>(controlPubTopic, queueSize);
  m_scanSub = n.subscribe(scanSubTopic, queueSize, &ScanControl::scanCallback, this);
}

ScanControl::~ScanControl()
{
}

float ScanControl::getSteeringAngle()
{
  return m_steeringAngle;
}

void ScanControl::setSteeringAngle(const float &steeringAngle)
{
  m_steeringAngle = steeringAngle; // change steering angle
}

ackermann_msgs::AckermannDriveStamped ScanControl::ackermannMsg()
{
  // Generate ackermann msg
  ackermann_msgs::AckermannDriveStamped ackMsg;
  ackMsg.header.stamp = ros::Time();
  ackMsg.drive.speed = m_VEL_X;
  ackMsg.drive.steering_angle = m_steeringAngle;

  return ackMsg;
}

void ScanControl::printRegionOfInterest(const std::vector<float> & ranges)
{
  std::ostringstream oss;

  if (!ranges.empty())
  {
    // Convert all but the last element to avoid a trailing ","
    std::copy(ranges.begin(), ranges.end()-1,
        std::ostream_iterator<int>(oss, ","));

    // Now add the last element with no delimiter
    oss << ranges.back();
  }

  ROS_INFO("Range vector: [%s]", oss.str().c_str());
}

void ScanControl::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg)
{
  printRegionOfInterest(scanMsg->ranges);                                                    // print result vector
  
  // Your desicion making algorithm
  m_steeringAngle = 0;
  
  m_controlPub.publish(ackermannMsg());
}