#include <fstream>
#include "ros/ros.h"
#include "ros/package.h"
#include "sensor_msgs/LaserScan.h"

void save_data(const sensor_msgs::LaserScan & data)
{
  // generate file path
  std::string path = ros::package::getPath("unior_scanning");
  std::ofstream outFile(path+"/config/upd_ranges.txt");
  // ROS_INFO("Result path: %s",  path.c_str()+"confi");
  // save range values
  for (const auto &e : data.ranges ) outFile << std::to_string(e) << "\n";
  // flush the data
  outFile.close();
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "loader");
  ros::NodeHandle save_ranges;

  boost::shared_ptr<sensor_msgs::LaserScan const> sharedLaserData;
  sensor_msgs::LaserScan laser_data;

  sharedLaserData = ros::topic::waitForMessage<sensor_msgs::LaserScan>("/scan", save_ranges);
  
  if(sharedLaserData != NULL){
    ROS_INFO("Data loaded");
    laser_data = *sharedLaserData;
  }
  
  save_data(laser_data);

  ros::spinOnce();

  return 0;
}