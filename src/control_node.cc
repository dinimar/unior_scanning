#include "scan_control.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_base");
  ros::NodeHandle n;

  ScanControl sc(n, "junior_car/ackermann_cmd", "rectificated_scan", 1000);
  // control_pub = n.advertise<ackermann_msgs::AckermannDriveStamped>(, 1000);
  // ros::Subscriber sub = n.subscribe("scan", 1000, scanCallback);

  ros::spin();

  return 0;
}