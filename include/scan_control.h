#include <vector>
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/LaserScan.h"
#include "ackermann_msgs/AckermannDriveStamped.h"
#include "std_msgs/String.h"
#include "ros/ros.h"
#include "std_msgs/String.h"

using std::vector;

class ScanControl
{
private:
    ros::Publisher m_controlPub;
    ros::Subscriber m_scanSub;
    ros::Subscriber m_odomSub;

    // Sensor values
    vector<float> m_regionOfInterest{std::vector<float>(50, 0)}; // range values of 50 angles

    // Control values
    static const int m_VEL_X = 8;
    float m_steeringAngle = 0;

    // Callbacks
    void scanCallback(const sensor_msgs::LaserScan::ConstPtr &scanMsg);

    // Control
    ackermann_msgs::AckermannDriveStamped ackermannMsg();

    // Misc
    void printRegionOfInterest(const std::vector<float> & ranges);

public:
    ScanControl(ros::NodeHandle &n,
                std::string controlTopic,
                std::string scanTopic,
                uint32_t queueSize);

    float getSteeringAngle();
    void setSteeringAngle(const float &steeringAngle);

    ~ScanControl();
};
